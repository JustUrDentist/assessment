# Assessment

## Table of Contents
1. [Installation / Requirements](#installation-requirements)
2. [Usage](#usage)
 
## Installation / Requirements
Create a new database. 
Create the necessary table with the help of the sql file "assessment.sql". 
Change the necessary information for a successful database connection in the 
"main.go" (line 19) in cmd and in the "service_test.go" (lines 17 and 89) in pkg/entities/activity.

Used external packages: 
<br>github.com/go-chi/chi
<br>github.com/jackc/pgx/v4
  
## Usage
Test the program by executing the test files 
(service_test.go in pkg/entities/activity, post_test.go in pkg/http/api/routes/callback)
 or execute the main.go file in cmd and the tester_service.go in cmd/testService.
