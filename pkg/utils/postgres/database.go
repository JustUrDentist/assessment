package postgres

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"time"
)

const (
	//DBUrl   = "postgresql://postgres:12345678@localhost:5432/assessement?pool_max_conns=30"
	DBUrlf  = "postgresql://%s:%s@%s:%s/%s?pool_max_conns=30"
	timeOut = 120 * time.Second
)

type DatabaseHelper struct {
	Conn  *pgxpool.Conn
	Tx    pgx.Tx
	Err   error
	Pool  *pgxpool.Pool
	inUse bool
}

func NewDatabaseHelper(pool *pgxpool.Pool) *DatabaseHelper {
	return &DatabaseHelper{
		Pool:  pool,
		inUse: false,
	}
}

func NewDatabaseUrl(username, password, host, port, database string) string {
	return fmt.Sprintf(DBUrlf, username, password, host, port, database)
}

func (db *DatabaseHelper) acquireConn() {
	db.Conn = nil

	timeOut := time.Now().Add(timeOut)

	for db.Conn == nil || time.Now().After(timeOut) {
		db.Conn, db.Err = db.Pool.Acquire(context.TODO())
	}
}

func (db *DatabaseHelper) Queryf(sql string, v ...interface{}) string {
	return fmt.Sprintf(sql, v...)
}

func (db *DatabaseHelper) Begin() {
	if !db.inUse {
		db.inUse = true
		db.acquireConn()
		db.Tx, db.Err = db.Conn.Begin(context.TODO())
	}
}

func (db *DatabaseHelper) Commit() {
	if db.inUse {
		db.Err = db.Tx.Commit(context.TODO())
		db.Conn.Release()
		db.Tx = nil
		db.inUse = false
	}
}

func (db *DatabaseHelper) Rollback() {
	if db.inUse {
		db.Err = db.Tx.Rollback(context.TODO())
		db.Conn.Release()
		db.Tx = nil
		db.inUse = false
	}
}
