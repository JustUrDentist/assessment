package queue

import (
	"fmt"
	"sync"
	"time"
)

var (
	instance IDQueue
	once     sync.Once
)

type IDQueue struct {
	ids []int
	mux sync.Mutex
}

func NewIDQueue() *IDQueue {
	once.Do(func() {
		instance = IDQueue{
			ids: make([]int, 0),
			mux: sync.Mutex{},
		}
	})

	return &instance
}

func (q *IDQueue) Push(id int) {
	q.mux.Lock()
	if !q.existInQueue(id) {
		q.ids = append(q.ids, id)
	}
	q.mux.Unlock()
}

func (q *IDQueue) Pull() (int, bool) {
	q.mux.Lock()

	lenQueue := len(q.ids)
	if lenQueue == 0 {
		q.mux.Unlock()
		return -1, false
	}

	id := q.ids[0]

	if lenQueue > 1 {
		q.ids = q.ids[1:lenQueue:lenQueue]
	} else {
		q.ids = make([]int, 0)
	}

	q.mux.Unlock()

	return id, true
}

func (q *IDQueue) existInQueue(id int) bool {
	for _, i := range q.ids {
		if i == id {
			return true
		}
	}

	return false
}

func (q *IDQueue) Size() int {
	return len(q.ids)
}

func (q *IDQueue) TimeDuration() {
	time1 := time.Now()
	wasEmpty := false
	for {
		if q.Size() != 0 && wasEmpty {
			time1 = time.Now()
			wasEmpty = false
		}
		if q.Size() == 0 && !wasEmpty {
			time2 := time.Now()
			fmt.Println(time2.Sub(time1))
			wasEmpty = true
		}
	}
}
