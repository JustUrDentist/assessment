package routes

import (
	"github.com/go-chi/chi"
	"gitlab.com/JustUrDentist/assessement/pkg/http/api/routes/callback"
)

func Routers() *chi.Mux {
	r := chi.NewRouter()

	r.Post("/callback", callback.HandleIDs)

	return r
}
