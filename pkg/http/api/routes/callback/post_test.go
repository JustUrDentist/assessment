package callback

import (
	"bytes"
	"gitlab.com/JustUrDentist/assessement/pkg/utils/queue"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestHandleIDs(t *testing.T) {
	var jsonStr = []byte(`{"object_ids": [1,2]}`)

	req, err := http.NewRequest("POST", "/callback", bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Fatal(err)
	}

	req.Header.Set("Content-Type", "application/json")
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(HandleIDs)
	handler.ServeHTTP(rr, req)
	status := rr.Code
	if status != http.StatusOK {
		t.Errorf("wrong status code. expected %d got %d", http.StatusOK, status)
	}

	expected := queue.NewIDQueue().Size()
	if expected != 2 {
		t.Errorf("wrong queue size. expected %d got %d", 2, expected)
	}

	expected, ok := queue.NewIDQueue().Pull()
	if !ok && expected != 1 {
		t.Errorf("got wrong id from queue. expected %d got %d", 1, expected)
	}

	expected, ok = queue.NewIDQueue().Pull()
	if !ok && expected != 2 {
		t.Errorf("got wrong id from queue. expected %d got %d", 2, expected)
	}
}
