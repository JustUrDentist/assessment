package callback

import (
	"encoding/json"
	"gitlab.com/JustUrDentist/assessement/pkg/http/api"
	"gitlab.com/JustUrDentist/assessement/pkg/utils/queue"
	"net/http"
)

type objectIDs struct {
	IDs []int `json:"object_ids"`
}

func HandleIDs(w http.ResponseWriter, r *http.Request) {
	var objIDs objectIDs

	idQueue := queue.NewIDQueue()

	err := json.NewDecoder(r.Body).Decode(&objIDs)
	if err != nil {
		api.ResponseWithError(w, http.StatusBadRequest, err.Error())
	}

	//add ids to the queue
	for _, id := range objIDs.IDs {
		idQueue.Push(id)
	}

	api.ResponseWithJSON(w, http.StatusOK, map[string]string{"msg": "success"})
}
