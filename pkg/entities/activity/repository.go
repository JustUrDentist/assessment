package activity

type Repository interface {
	InsertOrUpdateActivity(obj string) error
	DeleteActivity() error
	LastSeenInterval() (float64, error)
	AnyEntryExist() (bool, error)
	GetIDsByStatus(status bool) ([]int, error)
}
