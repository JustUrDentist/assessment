package activity

import (
	"context"
	"errors"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	dbHelper "gitlab.com/JustUrDentist/assessement/pkg/utils/postgres"
)

var (
	insertOrUpdatef  = "INSERT INTO activities (id, online, last_seen) VALUES %s ON CONFLICT (id) DO UPDATE SET last_seen = NOW();"
	deleteActivity   = "DELETE FROM activities WHERE last_seen < (NOW() - INTERVAL '30 SECOND')"
	lastSeenInterval = "SELECT EXTRACT(EPOCH FROM MIN(last_seen) - NOW()) FROM activities"
	anyEntryExist    = "SELECT COUNT(*) FROM activities"
	getIdsByStatusf  = "SELECT id FROM activities WHERE online = %t"
)

var NoEntryError = errors.New("no entry in table")

type postgres struct {
	pool *pgxpool.Pool
}

func NewPostgresRepository(p *pgxpool.Pool) Repository {
	return &postgres{
		pool: p,
	}
}

func (p *postgres) InsertOrUpdateActivity(obj string) error {
	db := dbHelper.NewDatabaseHelper(p.pool)

	db.Begin()
	if db.Err != nil {
		db.Rollback()
		return db.Err
	}

	_, db.Err = db.Conn.Exec(context.TODO(), db.Queryf(insertOrUpdatef, obj))
	if db.Err != nil {
		return db.Err
	}

	db.Commit()
	return nil
}

func (p *postgres) DeleteActivity() error {
	db := dbHelper.NewDatabaseHelper(p.pool)

	db.Begin()
	if db.Err != nil {
		db.Rollback()
		return db.Err
	}

	_, db.Err = db.Conn.Exec(context.TODO(), deleteActivity)
	if db.Err != nil {
		return db.Err
	}

	db.Commit()
	if db.Err != nil {
		db.Rollback()
		return db.Err
	}
	return nil
}

func (p *postgres) LastSeenInterval() (float64, error) {
	db := dbHelper.NewDatabaseHelper(p.pool)
	var exist bool
	var rows pgx.Rows
	var result float64

	exist, db.Err = p.AnyEntryExist()

	if !exist {
		return -1, NoEntryError
	}

	db.Begin()
	if db.Err != nil {
		db.Rollback()
		return -1, db.Err
	}

	rows, db.Err = db.Conn.Query(context.TODO(), lastSeenInterval)
	if db.Err != nil {
		return -1, db.Err
	}

	for rows.Next() {
		db.Err = rows.Scan(&result)
		if db.Err != nil {
			return -1, db.Err
		}
	}

	db.Commit()
	return result, nil
}

func (p *postgres) AnyEntryExist() (bool, error) {
	db := dbHelper.NewDatabaseHelper(p.pool)
	var rows pgx.Rows
	var result int

	db.Begin()
	if db.Err != nil {
		db.Rollback()
		return false, db.Err
	}

	rows, db.Err = db.Conn.Query(context.TODO(), anyEntryExist)
	if db.Err != nil {
		return false, db.Err
	}

	for rows.Next() {
		db.Err = rows.Scan(&result)
		if db.Err != nil {
			return false, db.Err
		}
	}

	db.Commit()
	return result > 0, nil
}

func (p *postgres) GetIDsByStatus(status bool) ([]int, error) {
	db := dbHelper.NewDatabaseHelper(p.pool)
	var rows pgx.Rows
	var id int
	var result []int

	db.Begin()
	if db.Err != nil {
		db.Rollback()
		return nil, db.Err
	}

	rows, db.Err = db.Conn.Query(context.TODO(), db.Queryf(getIdsByStatusf, status))
	if db.Err != nil {
		return nil, db.Err
	}

	for rows.Next() {
		db.Err = rows.Scan(&id)
		if db.Err != nil {
			return nil, db.Err
		}
		result = append(result, id)
	}

	db.Commit()
	return result, nil
}
