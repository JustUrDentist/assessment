package activity

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/JustUrDentist/assessement/pkg/utils/queue"
	"net/http"
	"strconv"
	"time"
)

type Activity struct {
	repo Repository
}

func NewActivityService(repo Repository) Service {
	return &Activity{
		repo: repo,
	}
}

func (a *Activity) Delete() error {
	//get the time of the oldest entry
	waitTime, err := a.repo.LastSeenInterval()
	if err != nil {
		if err == NoEntryError {
			time.Sleep(time.Duration(1) * time.Second)
			return nil
		}
		return err
	}

	//if the time is greater 0 wait and return else delete entries older than 30 sec
	if waitTime > 0 {
		time.Sleep(time.Duration(waitTime) * time.Second)
		return nil
	}
	err = a.repo.DeleteActivity()
	if err != nil {
		return err
	}

	return nil
}

func (a *Activity) GetOnlineObjects(client *http.Client) error {
	//connect to endpoint and get object infos
	var objState objectState
	err := a.clientConnect(client, &objState)
	if err != nil {
		return err
	}

	//insert or update object infos
	err = a.repo.InsertOrUpdateActivity(objState.toSQLString())
	if err != nil {
		return err
	}
	return nil
}

func (a *Activity) clientConnect(client *http.Client, obj *objectState) error {
	id, ok := queue.NewIDQueue().Pull()
	if !ok {
		time.Sleep(400 * time.Millisecond)
		return nil
	}

	resp, err := client.Post(fmt.Sprintf("http://localhost:9010/objects/%d", id), "application/x-www-form-urlencoded", nil)
	if err != nil {
		return err
	}

	if resp == nil {
		return errors.New("response is nil")
	}

	err = json.NewDecoder(resp.Body).Decode(obj)
	if err != nil {
		return err
	}

	_ = resp.Body.Close()

	return nil
}

type objectState struct {
	ID     int  `json:"id"`
	Online bool `json:"online"`
}

func (obj *objectState) toSQLString() string {
	return "(" + strconv.Itoa(obj.ID) + ", " + strconv.FormatBool(obj.Online) + ", NOW())"
}
