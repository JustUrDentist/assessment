package activity

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v4/pgxpool"
	dbHelper "gitlab.com/JustUrDentist/assessement/pkg/utils/postgres"
	"gitlab.com/JustUrDentist/assessement/pkg/utils/queue"
	"net/http"
	"strconv"
	"strings"
	"testing"
	"time"
)

func TestActivity_Delete(t *testing.T) {
	pool, err := pgxpool.Connect(context.TODO(), dbHelper.NewDatabaseUrl("postgres", "12345678", "localhost", "5432", "assessement"))
	if err != nil {
		t.Fatal(err)
	}
	defer pool.Close()

	activityRepo := NewPostgresRepository(pool)
	activityService := NewActivityService(activityRepo)

	go func() {
		for {
			err = activityService.Delete()
			if err != nil {
				t.Fatal(err)
			}
		}
	}()

	objs := []objectState{
		{
			ID:     1,
			Online: false,
		},
		{
			ID:     2,
			Online: false,
		},
	}

	err = activityRepo.InsertOrUpdateActivity(objs[0].toSQLString())
	if err != nil {
		t.Fatal(err)
	}
	err = activityRepo.InsertOrUpdateActivity(objs[1].toSQLString())
	if err != nil {
		t.Fatal(err)
	}

	expectation, _ := activityRepo.AnyEntryExist()

	if !expectation {
		t.Errorf("no entry exist. expected %t got %t", !expectation, expectation)
	}

	time.Sleep(31 * time.Second)

	expectation, _ = activityRepo.AnyEntryExist()

	if expectation {
		t.Errorf("entry exist. expected %t got %t", !expectation, expectation)
	}
}

func TestActivity_GetOnlineObjects(t *testing.T) {
	http.HandleFunc("/objects/", func(w http.ResponseWriter, r *http.Request) {
		time.Sleep(time.Duration(300) * time.Millisecond)

		//fixed problem, missing 's' in string 'object'
		idRaw := strings.TrimPrefix(r.URL.Path, "/objects/")
		id, err := strconv.Atoi(idRaw)
		if err != nil {
			http.Error(w, "invalid id", http.StatusBadRequest)
			return
		}

		_, err = w.Write([]byte(fmt.Sprintf(`{"id":%d,"online":%v}`, id, id%2 == 0)))
		if err != nil {
			t.Fatal(err)
		}
	})
	go func() { _ = http.ListenAndServe(":9010", nil) }()

	pool, err := pgxpool.Connect(context.TODO(), dbHelper.NewDatabaseUrl("postgres", "12345678", "localhost", "5432", "assessement"))
	if err != nil {
		t.Fatal(err)
	}
	defer pool.Close()

	activityRepo := NewPostgresRepository(pool)
	activityService := NewActivityService(activityRepo)

	go func() {
		for {
			client := &http.Client{}
			err = activityService.GetOnlineObjects(client)
			if err != nil {
				t.Fatal(err)
			}
		}
	}()

	queue.NewIDQueue().Push(4)

	time.Sleep(2 * time.Second)

	expectation, _ := activityRepo.AnyEntryExist()
	if !expectation {
		t.Errorf("no entryExist expected %t got %t", !expectation, expectation)
	}
}
