package activity

import "net/http"

type Service interface {
	Delete() error
	GetOnlineObjects(client *http.Client) error
}
