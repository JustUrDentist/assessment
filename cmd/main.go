package main

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/JustUrDentist/assessement/pkg/entities/activity"
	"gitlab.com/JustUrDentist/assessement/pkg/http/api/routes"
	"gitlab.com/JustUrDentist/assessement/pkg/utils/postgres"
	"gitlab.com/JustUrDentist/assessement/pkg/utils/queue"
	"net/http"
)

const (
	amountGoRoutines = 25
)

func main() {
	pool, err := pgxpool.Connect(context.TODO(), postgres.NewDatabaseUrl("postgres", "12345678", "localhost", "5432", "assessement"))
	if err != nil {
		panic(err)
	}
	defer pool.Close()

	activityRepo := activity.NewPostgresRepository(pool)
	activityService := activity.NewActivityService(activityRepo)

	//start the go routines to empty the queue and fill the database table
	for i := 0; i < amountGoRoutines; i++ {
		go func() {
			for {
				client := &http.Client{}
				err = activityService.GetOnlineObjects(client)
				if err != nil {
					fmt.Println(err)
				}
			}
		}()

	}

	//start the go routine to delete entries older than 30 seconds
	go func() {
		for {
			err = activityService.Delete()
			if err != nil {
				fmt.Println(err)
			}
		}
	}()

	//returns the time it took to empty the queue
	go func() {
		q := queue.NewIDQueue()
		q.TimeDuration()
	}()

	r := routes.Routers()
	err = http.ListenAndServe(":9090", r)
	if err != nil {
		fmt.Println(err)
	}
}
