CREATE TABLE "activities" (
  "id" int PRIMARY KEY,
  "online" boolean NOT NULL,
  "last_seen" timestamp NOT NULL
);
