module gitlab.com/JustUrDentist/assessement

go 1.14

require (
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/jackc/pgx/v4 v4.9.0
)
